require 'uri'
require 'net/http'
require 'openssl'
require 'pry'


def generate_xrfkey number
  charset = Array('A'..'Z') + Array('a'..'z') + Array(0..9)
  Array.new(number) { charset.sample }.join
end

xrfkey = generate_xrfkey(16)
url = URI("https://192.168.12.32:4243/qps/ticket?Xrfkey=#{xrfkey}")
http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.cert = OpenSSL::X509::Certificate.new(File.read("./certs/client.pem"))
http.key =  OpenSSL::PKey::RSA.new(File.read("./certs/client_key.pem"))
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Post.new(url)
request["x-qlik-xrfkey"] = "#{xrfkey}"
request["content-type"] = 'application/json'
request["cache-control"] = 'no-cache'
request.body = "{\n\t\"UserDirectory\" : \"QLIK\",\n\t\"UserId\": \"ti\",\n\t\"Attributes\" : []\n}"
response = http.request(request)
puts response.read_body

